import { createRouter, createWebHistory } from 'vue-router';
import store from '../store';
import Home from '../components/pages/Home.vue';
import PostAdd from '../components/posts/PostAdd.vue';
import Post from '../components/posts/Post.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: '/', component: Home },
    {
      path: '/post/add',
      name: 'AddPost',
      component: PostAdd,
      meta: { requiresAuth: true }
    },
    {
      path: '/post/:type/:id',
      name: 'Post',
      component: Post,
    }
  ]
});

router.beforeEach((to, from, next) => {
  const isUserLoggedIn = !!localStorage.getItem('token');
  store.commit('SET_AUTH', isUserLoggedIn);
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!isUserLoggedIn) {
      store.commit('SET_BANNER', 'login');
      next({
        path: '/',
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
