import { createApp, provide, h } from 'vue';
import { DefaultApolloClient } from '@vue/apollo-composable';
import apolloClient from './apollo';
import router from './router';
import store from './store';

import App from './App.vue';

const app = createApp({
  setup () {
      provide(DefaultApolloClient, apolloClient)
    },
    render() {
      return h(App)
    }
  })
  .use(router)
  .use(store);

app.config.globalProperties.$log = console.log;
app.mount('#app');
