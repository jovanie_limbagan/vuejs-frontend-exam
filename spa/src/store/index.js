import { createStore } from 'vuex';

const store = createStore({
  state() {
    return {
      activeBanner: '',
      isAuthenticated: false,
      user: {},
    };
  },
  getters: {
    isAuthenticated: state => {
      return state.isAuthenticated
    },
    user: state => {
      return state.user
    },
    activeBanner: state => {
      return state.activeBanner
    }
  },
  mutations: {
    SET_TOKEN (state, token) {
      state.token = token;
      localStorage.setItem('token', token);
    },
    SET_AUTH (state, value) {
      state.isAuthenticated = value;
    },
    LOGIN_USER (state, user) {
      state.user = { ...user };
    },
    LOGOUT_USER () {
      localStorage.removeItem('token');
    },
    SET_BANNER (state, value) {
      state.activeBanner = value;
    }
  },
  actions: {
    logOut({commit}) {
      commit('LOGOUT_USER');
      commit('SET_AUTH', false);
    },
  }
});

export default store;
