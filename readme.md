# L I G Frontend Exam

Frontend exam for LIG applicants.

## Implementation Level
### Senior Level
- Create all pages and API integration.

## Environment Setup
```
docker-compose build
```
Start the containers
```
docker-compose up -d
```
Run this command to login to docker container
```
docker exec -it CONTAINER_NAME bash
```
